#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------
"""
This module will process pairs of integers and for each pair determine the
maximum cycle length over all integers between and i
"""

# pylint: disable = invalid-name

from typing import IO, List, Dict

# ------------
# collatz_read
# ------------


def collatz_read(string: str) -> List[int]:
    """
    read   two ints
    string a string
    return a list of two ints, representing the beginning and end of a range, [beg, end]
    """
    arr = string.split()
    return [int(arr[0]), int(arr[1])]

# ------------
# collatz_eval
# ------------


# cache previously calc'd numbers
COLLATZ_CACHE: Dict[int, int] = {}


def collatz_function(num: int) -> int:
    """
    num the value that we are calculating num_cycles for
    return the num_cycles for num
    """
    result = 1
    original = num
    while num != 1:
        if num in COLLATZ_CACHE:
            result += COLLATZ_CACHE[num] - 1
            break
        # odd-number optimization
        if num % 2 == 1:
            num += (num >> 1) + 1
            result += 2
        else:
            num = num >> 1
            result += 1
    COLLATZ_CACHE[original] = result
    return result


def find_max(beg: int, end: int) -> int:
    """
    beg the beginning of the range, inclusive
    end the end       of the range, inclusive
    return the max_cycle cycle length of the range [beg, end]
    """
    max_cycle = 1
    for num in range(beg, end + 1):
        curmax = collatz_function(num)
        if max_cycle < curmax:
            max_cycle = curmax

    # post-condition check
    assert max_cycle > 0
    return max_cycle


def collatz_eval(beg: int, end: int) -> int:
    """
    beg the beginning of the range, inclusive
    end the end       of the range, inclusive
    return the max_cycle cycle length of the range [beg, end]
    """
    # pre-conditions
    assert 0 < beg <= 1000000
    assert 0 < end <= 1000000

    if beg > end:
        (beg, end) = (end, beg)

    # optimization
    if beg < end // 2:
        beg = end // 2

    return find_max(beg, end)

# -------------
# collatz_print
# -------------


def collatz_print(write: IO[str], beg: int, end: int, max_cycle: int) -> None:
    """
    print    	  three ints
    write   	  a writer
    beg     	  the beginning of the range, inclusive
    end     	  the end of the range, inclusive
    max_cycle     the max cycle length
    """
    write.write(str(beg) + " " + str(end) + " " + str(max_cycle) + "\n")

# -------------
# collatz_solve
# -------------

def collatz_solve(read: IO[str], write: IO[str]) -> None:
    """
    read a reader
    write a writer
    """
    for string in read:
        beg, end = collatz_read(string)
        max_cycle = collatz_eval(beg, end)
        collatz_print(write, beg, end, max_cycle)
