#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_function

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        string = "1 10\n"
        beg, end = collatz_read(string)
        self.assertEqual(beg, 1)
        self.assertEqual(end, 10)

    # ----
    # eval
    # ----

    def test_collatz_function_1(self):
        val = collatz_function(1)
        self.assertEqual(val, 1)

    def test_collatz_function_2(self):
        val = collatz_function(2)
        self.assertEqual(val, 2)

    def test_collatz_function_3(self):
        val = collatz_function(3)
        self.assertEqual(val, 8)

    def test_collatz_function_4(self):
        val = collatz_function(4)
        self.assertEqual(val, 3)

    def test_collatz_function_5(self):
        val = collatz_function(5)
        self.assertEqual(val, 6)

    def test_collatz_function_6(self):
        val = collatz_function(6)
        self.assertEqual(val, 9)

    def test_eval_1(self):
        val = collatz_eval(1, 10)
        self.assertEqual(val, 20)

    def test_eval_2(self):
        val = collatz_eval(100, 200)
        self.assertEqual(val, 125)

    def test_eval_3(self):
        val = collatz_eval(201, 210)
        self.assertEqual(val, 89)

    def test_eval_4(self):
        val = collatz_eval(900, 1000)
        self.assertEqual(val, 174)

    def test_eval_backwards_numbers(self):
        val = collatz_eval(1000, 900)
        self.assertEqual(val, 174)

    # -----
    # print
    # -----

    def test_print(self):
        write = StringIO()
        collatz_print(write, 1, 10, 20)
        self.assertEqual(write.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        read = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        write = StringIO()
        collatz_solve(read, write)
        self.assertEqual(
            write.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
